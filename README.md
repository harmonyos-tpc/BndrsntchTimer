# BndrsntchTimer

BndrsntchTimer : A horizontal progress bar shrinking with time; similar to Bandersnatch choice interface.

# Inspiration
<img src="./images/bandersnatch_insp.png" width=450 ></img>

# Preview
<img src="./images/bndrsntch_output.png" width=450 ></img>

# Supported features
* Timer having a horizontal progressbar shrinking wrt elapsedTime
* Change the progressbar color can be implemented
* Duration of the timer can be implemented
* Able to get the RunningStatus and TotalDuration time
* Reset the timer for the repeated usage

# Usage Instructions
Add namespace for app in layout root.
```
xmlns:app="http://schemas.huawei.com/hap/res-auto"

```

# Layout xml
```
    <com.prush.bndrsntchtimer.BndrsntchTimer
        ohos:id="$+id:timer"
        ohos:width="match_parent"
        ohos:height="4vp"
        ohos:align_parent_top="true"
        ohos:align_bottom="8vp"
        ohos:padding="10vp"
        app:progress_color="$color:white"
        />
```

Add two text buttons at two ends .
Left : Reset the timer
Right : Initiate the timer
```
   <Text
        ohos:id="$+id:text_leftChoiceTextView"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_parent_left="true"
        ohos:align_parent_start="true"
        ohos:align_bottom="$id:timer"
        ohos:layout_alignment="center"
        ohos:text="$string:left_text"
        ohos:text_color="$color:darker_gray"
        ohos:text_size="16vp"
        />

    <Text
        ohos:id="$+id:text_rightChoiceTextView"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_parent_end="true"
        ohos:align_parent_right="true"
        ohos:align_bottom="$id:timer"
        ohos:layout_alignment="center"
        ohos:text="$string:right_text"
        ohos:text_color="$color:darker_gray"
        ohos:text_size="16vp"
        />
```



# Java
```java
final BndrsntchTimer bndrsntchTimer = findViewById( R.id.timer );

// add the lifecycle observer
getLifecycle().addObserver( bndrsntchTimer.getLifecycleObserver() );

// set progress color
bndrsntchTimer.setProgressColor(Color.WHITE);

bndrsntchTimer.start( 10000 );

// attach listener
bndrsntchTimer.setOnTimerElapsedListener( new BndrsntchTimer.OnTimerElapsedListener()
{
    @Override
    public void onTimeElapsed( long elapsedDuration, long totalDuration )
    {
        if( elapsedDuration >= totalDuration )
        {
            // Timer elapsed.
        }
    }
});

// reset it for repeated usage.
bndrsntchTimer.reset();

// set duration along with listener
bndrsntchTimer.start( 5000, new BndrsntchTimer.OnTimerElapsedListener()
{
    @Override
    public void onTimeElapsed( long elapsedDuration, long totalDuration )
    {
        if( elapsedDuration >= totalDuration )
        {
            // Timer elapsed.
        }
    }
});

```
# Installation Instructions
1.For using BndrsntchTimer module in sample application, include the below library dependency to generate hap/bndrsntchtimer_library.har.

Modify entry build.gradle as below :
```
dependencies {
    implementation project(path: ':bndrsntchtimer_library')
}
```

2.For using BndrsntchTimer in separate application, add the "bndrsntchtimer_library.har" in libs folder of "entry" module.

Modify entry build.gradle as below :

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
}
```
    
3.For using BndrsntchTimer from a remote repository in separate application, add the below dependency in entry/build.gradle file.
 
 Modify entry build.gradle as below :
             
             
             ```gradle
             dependencies {
                    implementation 'io.openharmony.tpc.thirdlib:BndrsntchTimer:1.0.0'
             }
             ```


# License

```
Copyright 2019 Purushottam Pawar

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```
