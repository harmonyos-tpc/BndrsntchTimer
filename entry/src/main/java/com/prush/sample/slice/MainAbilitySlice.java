/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prush.sample.slice;

import java.util.Random;

import ohos.aafwk.ability.AbilitySlice;

import ohos.aafwk.content.Intent;

import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;

import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;

import ohos.agp.window.dialog.ToastDialog;

import com.prush.bndrsntchtimer.BndrsntchTimer;
import com.prush.bndrsntchtimer.LogUtil;

import com.prush.sample.ResourceTable;

/**
 * MainAbilitySlice setting the BndrsntchTimer.
 */
public class MainAbilitySlice extends AbilitySlice implements  Component.ClickedListener {
    private static final String TAG = "BndrsntchTimer";
    private BndrsntchTimer mBndrsntchTimer;
    private Text mLeftChoiceView;
    private Text mRightChoiceView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);

        mBndrsntchTimer = (BndrsntchTimer) findComponentById(ResourceTable.Id_timer);
        mLeftChoiceView = (Text) findComponentById(ResourceTable.Id_text_leftChoiceTextView);
        mRightChoiceView = (Text) findComponentById(ResourceTable.Id_text_rightChoiceTextView);

        mLeftChoiceView.setClickedListener(this::onClick);
        mRightChoiceView.setClickedListener(this::onClick);

        mBndrsntchTimer.setOnTimerElapsedListener(new BndrsntchTimer.OnTimerElapsedListener() {
            @Override
            public void onTimeElapsed(long elapsedDuration, long totalDuration) {
                if (elapsedDuration >= totalDuration) {
                    Random random = new Random();
                    int choice = random.nextInt(2);
                    selectChoiceTextBtn(choice);
                    showToast("Click on Left choice to reset timer, Right choice to start again.");
                }
            }
        });
        getLifecycle().addObserver(mBndrsntchTimer.getLifecycleObserver());
        mBndrsntchTimer.start(10000);
    }

    private void selectChoiceTextBtn(int choice) {
        if (choice == 0) {
            mLeftChoiceView.setTextColor(Color.WHITE);
        } else {
            mRightChoiceView.setTextColor(Color.WHITE);
        }
    }

    private void showToast(String toastText) {
        Text text = new Text(this);
        text.setMultipleLine(true);
        text.setText(toastText);
        text.setTextSize(48);

        DirectionalLayout mainLayout = new DirectionalLayout(this);
        mainLayout.addComponent(text);

        ToastDialog toastDialog = new ToastDialog(this);
        toastDialog.setComponent((Component) mainLayout);
        toastDialog.setSize(1500, 100);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.setDuration(2000);
        toastDialog.show();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_text_leftChoiceTextView:
                mBndrsntchTimer.reset(new BndrsntchTimer.OnTimerResetListener() {
                    @Override
                    public void onTimerResetCompleted() {
                        LogUtil.debug(TAG, "onTimerResetCompleted: ");
                    }
                });
                mLeftChoiceView.setTextColor(Color.WHITE);
                break;

            case ResourceTable.Id_text_rightChoiceTextView:
                mBndrsntchTimer.start(10000);
                mRightChoiceView.setTextColor(Color.WHITE);
                break;
        }
    }
}